package com.example.preexamen;

public class ReciboNomina {

private int numRecibo= 0;
private String Nombre="";
private float horasTrabNormal=0.0f;
private float horasTrabExtras= 0.0f;
private int puesto = 0;
private float impuestoPorc = 0.0f;

public ReciboNomina(){
    this.Nombre="";
    this.horasTrabExtras=0.0f;
    this.horasTrabNormal=0.0f;
    this.numRecibo=0;
    this.impuestoPorc=0.0f;
    this.puesto=0;
}

public int getNumRecibo(){
    return numRecibo;
}

public void setNumRecibo(int numRecibo){
    this.numRecibo = numRecibo;
}

public float getHorasTrabNormal(){
    return horasTrabNormal;
}

public void setHorasTrabNormal(float HorasTrabNormal){
    this.horasTrabNormal = horasTrabNormal;
}
public int getPuesto(){
    return puesto;
}

public void setPuesto(int puesto){
    this.puesto = puesto;
}

public float getImpuestoPorc(){
    return impuestoPorc;
}

public void setImpuestoPorc(float impuestoPorc){
    this.impuestoPorc = impuestoPorc;
}

public float getHorasTrabExtras(){
    return horasTrabExtras;
}

public void setHorasTrabExtras(float horasTrabExtras){
    this.horasTrabExtras = horasTrabExtras;
}

public String getNombre(){
    return Nombre;
}

public void setNombre(){
    this.Nombre = Nombre;
}

public float Calcular(float pagar){
    float Res;
    Res = pagar * horasTrabNormal;
    if(horasTrabExtras > 0){
        Res = Res + (horasTrabExtras * pagar * 2);
    }
    return  Res;
}

public float CalcularSubtotal(){
    float Sub = 0.0f;
    switch (puesto){
        case 1: Sub = Calcular(240);
        return Sub;
        case 2: Sub = Calcular(300);
        return Sub;
        case 3: Sub = Calcular(400);
        return Sub;
    }
    return Sub;
}

public float CalcularImpuesto(float Subs){
    impuestoPorc = Subs * 0.16F;
    return impuestoPorc;
}

public float CalcularTotal(float impuestoPorc, float Subst)
{
    return Subst - impuestoPorc;
}


}

