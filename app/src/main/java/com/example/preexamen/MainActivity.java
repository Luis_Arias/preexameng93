package com.example.preexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button BtnSalir;
    private Button BtnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        BtnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        BtnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Salir();
            }
        });

    }

    private void iniciarComponentes(){
        txtNombre = findViewById(R.id.txtNombre);
        BtnEntrar = findViewById(R.id.btnEntrar);
        BtnSalir = findViewById(R.id.btnSalir);
    }

    private void ingresar(){
        if(txtNombre.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),
                    "Ingrese su nombre", Toast.LENGTH_LONG).show();
        }else{
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombre.getText().toString());
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            txtNombre.setText("");
        }
    }

    private void Salir()
    {
        finishAndRemoveTask();
    }
}