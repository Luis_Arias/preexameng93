package com.example.preexamen;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.os.Bundle;
import android.content.DialogInterface;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private TextView lblNumRecibo;
    private TextView lblNombre;
    private TextView lblNombreMain;
    private EditText txtHorasNormal;
    private EditText txtHorasExtras;
    private RadioButton rdbAuxiliar;
    private RadioButton rdbAlbanil;
    private RadioButton rdbIngObra;
    private RadioGroup radioGrupo;
    private TextView lblImpuestoPor;
    private TextView lblSubtotal;
    private TextView lblimpuesto;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private ReciboNomina Recibo = new ReciboNomina();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Regresar();
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calcular();
            }
        });

    }

    public void iniciarComponentes() {
        lblimpuesto = findViewById(R.id.lblimpuesto);
        lblImpuestoPor = findViewById(R.id.lblImpuestoPor);
        lblNombre = findViewById(R.id.lblNombre);
        lblNombre.setText(lblNombre.getText().toString() + getIntent().getStringExtra("nombre"));
        lblTotal = findViewById(R.id.lblTotal);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombreMain = findViewById(R.id.lblNombreMain);

        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        radioGrupo = findViewById(R.id.grupoBotones);
        rdbAlbanil = findViewById(R.id.rdbAlbanil);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        rdbIngObra = findViewById(R.id.rdbIngObra);
    }

    private void CalculoRPP() {
        Recibo.setHorasTrabNormal(Float.parseFloat(txtHorasNormal.getText().toString()));
        Recibo.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
        lblSubtotal.setText(String.valueOf(Recibo.CalcularSubtotal()));
        lblImpuestoPor.setText(String.valueOf(Recibo.CalcularImpuesto(Float.parseFloat(lblSubtotal.getText().toString()))));
        lblTotal.setText(String.valueOf(Recibo.CalcularTotal(Float.parseFloat(lblImpuestoPor.getText().toString()), Float.parseFloat(lblSubtotal.getText().toString()))));
    }

    private void Calcular() {
        boolean Comprobar = rdbAuxiliar.isChecked() == false && rdbAlbanil.isChecked() == false && rdbIngObra.isChecked() == false;

        if (lblNumRecibo.getText().toString().trim().isEmpty() ||
                lblNombre.getText().toString().trim().isEmpty() ||
                txtHorasNormal.getText().toString().trim().isEmpty() ||
                txtHorasExtras.getText().toString().trim().isEmpty() || Comprobar) {
            Toast.makeText(getApplicationContext(),
                    "Falta Ingresar Datos", Toast.LENGTH_LONG).show();
        } else {
            if (rdbAuxiliar.isChecked()) {
                Recibo.setPuesto(1);
                CalculoRPP();
            } else if (rdbAlbanil.isChecked()) {
                Recibo.setPuesto(2);
                CalculoRPP();
            } else if (rdbIngObra.isChecked()) {
                Recibo.setPuesto(3);
                CalculoRPP();
            }
        }

    }

    private void Regresar(){
        AlertDialog.Builder Salida = new AlertDialog.Builder(this);
        Salida.setTitle("Saliendo..");
        Salida.setMessage(" ¿Seguro de regresar? ");
        Salida.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        Salida.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Salida.show();
    }



    private void Limpiar(){
        lblNumRecibo.setText("");
        txtHorasNormal.setText("");
        txtHorasExtras.setText("");
        lblImpuestoPor.setText("");
        lblSubtotal.setText("");
        lblTotal.setText("");
    }


}
